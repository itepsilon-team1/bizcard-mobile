import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../components/pages/login/Login';
import Register from '../components/pages/login/Regsiter';
import ForgetPassword from '../components/pages/login/ForgetPassword';
import FinishRegister from '../components/pages/login/FinishRegister';
import Cards from '../components/pages/auth/Card';
import Me from '../components/pages/auth/Me';
import BizFooter from '../components/BizFooter';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

function MyTabs() {
	return (
		<Tab.Navigator tabBar={props => <BizFooter {...props}/>}>
			<Tab.Screen name="Me" component={Me} />
            <Tab.Screen name="Card" component={Cards} options={{ headerShown: true, headerStyle: {backgroundColor: '#2b92f1'}, headerTintColor: '#fff' }}/>
		</Tab.Navigator>
	);
}

const Stack = createStackNavigator();
const MasterNavigation = () => {
    return (
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Tabs" component={MyTabs} options={{ headerShown: false }}/>
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
            <Stack.Screen name="ForgetPassword" component={ForgetPassword} options={{ headerShown: false }}/>
            <Stack.Screen name="FinishRegister" component={FinishRegister} options={{ headerShown: false }}/>
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }}/>
        </Stack.Navigator>
    )
}

export default MasterNavigation;