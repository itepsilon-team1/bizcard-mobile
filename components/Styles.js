'use strict'
import {StyleSheet} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1
    },
    container: { flex: 1 },
    action: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        marginBottom: hp('5%'),
    },
    inputContainer: {
        marginTop: hp('5%')
    },
    textLogin: {
        paddingLeft: 10,
        color: '#05375a',
        paddingBottom: hp('1%'),
        fontSize: hp('2.5%'),
    },
    SubmitButtonStyle: {
        padding: wp('3%'),
        width: wp('50%'),
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#fff'
    },
    TextStyle:{
        color:'#fff',
        textAlign:'center',
    },
    menuActive: {
        color: '#2b92f1'
    }
});

export {styles};