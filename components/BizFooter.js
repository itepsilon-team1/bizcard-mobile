import React from 'react';
import {Footer, FooterTab, Button, Icon} from 'native-base';
import {Text, StyleSheet, Dimensions} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const BizFooter = ({ state, descriptors, navigation }) => {
    return (
        <Footer>
            <FooterTab style={{backgroundColor: '#fff'}}>
                <Button info transparent vertical style={state.index == 1 ? styles.menuActive : styles.menuIdle}>
                    <Icon name="card" />
                    <Text style={styles.textMenu} onPress={()=> {navigation.navigate('Card')}}>Cards</Text>
                </Button>
                {/* style={props.active == 'sync' ? styles.menuActive : styles.menuIdle} */}
                <Button info transparent vertical style={state.index == 2 ? styles.menuActive : styles.menuIdle}>
                    <Icon name="cloud-outline" />
                    <Text style={styles.textMenu}>Sync</Text>
                </Button>
                <Button info rounded style={{flex: 0, marginBottom: 50, width:Dimensions.get('window').height / 10}}>
                    <Icon name="qr-scanner" style={{color: '#fff'}}/>
                </Button>
                <Button info transparent vertical style={state.index == 3 ? styles.menuActive : styles.menuIdle}>
                    <Icon name="chatbubbles" />
                    <Text style={styles.textMenu}>Exchange</Text>
                </Button>
                <Button info transparent vertical style={state.index == 0 ? styles.menuActive : styles.menuIdle}>
                    <Icon name="person" />
                    <Text style={styles.textMenu} onPress={()=> {navigation.navigate('Me')}}>Me</Text>
                </Button>
            </FooterTab>
        </Footer>
    )
}

const styles = StyleSheet.create({
    textMenu: {
        color: '#3faafc',
        fontSize: Dimensions.get('window').width / 30
    },
    menuIdle: {
        opacity: .5
    }
})

export default BizFooter;