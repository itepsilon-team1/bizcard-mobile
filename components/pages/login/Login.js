import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from "react-native-elements";
import { styles as pstyles } from '../../Styles';
import {LinearGradient} from 'expo-linear-gradient';
import React from 'react';

const Login = props => {
    return (
      <LinearGradient colors={['#3ea9fb', '#3ea9fb', '#0468e2']} style={pstyles.linearGradient}>
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Text style={styles.myText}>BizCard</Text>
            <Image style={styles.logoImage} source={require('../../../assets/icon/logo-p.png')}/>
          </View>
          <View style={pstyles.inputContainer}>
            <View style={pstyles.action}>
                <Icon 
                    name="account-circle"
                    color='white'
                    size={25}
                />
                <TextInput 
                    placeholder="Username"
                    placeholderTextColor="lightgrey"
                    style={[pstyles.textLogin, {
                        color: 'white'
                    }]}
                    autoCapitalize="none"
                />
            </View>
            <View style={pstyles.action}>
                <Icon 
                    name="lock"
                    color='white'
                    size={25}
                />
                <TextInput 
                    placeholder="Password"
                    placeholderTextColor="lightgrey"
                    style={[pstyles.textLogin, {
                        color: 'white'
                    }]}
                    autoCapitalize="none"
                />
            </View>
            <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'flex-end'}]}>
              <TouchableOpacity onPress={ () => props.navigation.navigate('ForgetPassword')}><Text style={{color: '#fff'}}>Forgot Password</Text></TouchableOpacity>
            </View>
            <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'center'}]}>
              <TouchableOpacity
                style={pstyles.SubmitButtonStyle}
                activeOpacity = { .5 }
                onPress={ () => props.navigation.navigate('Tabs')}
              >
                <Text style={pstyles.TextStyle}> Login </Text>    
              </TouchableOpacity>
            </View>
            <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'center'}]}>
              <Text style={{color: "white", textAlign:'center'}}>Don't have an account ? </Text><TouchableOpacity onPress={ () => props.navigation.navigate('Register')}><Text style={{fontWeight:'700', color: "white", textAlign:'center'}}>Sign Up</Text></TouchableOpacity>
            </View>
          </View>
        </View>
      </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1,paddingHorizontal: wp('8%') },
    logoContainer: {
      height: hp('40%'), // 70% of height device screen
      paddingTop: hp('10%'),
      alignItems: 'center'
    },
    myText: {
      fontSize: hp('7%'), // End result looks like the provided UI mockup
      color: 'white'
    },
    logoImage: {
      height: hp('12%'),
      width: wp('30%'),
      marginTop: hp('2%'),
      resizeMode: 'stretch'
    }
});

export default Login;