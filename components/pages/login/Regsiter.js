import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from "react-native-elements";
import React from 'react';
import { styles as pstyles } from '../../Styles';
import {LinearGradient} from 'expo-linear-gradient';

const Register = props => {
    return (
    <LinearGradient colors={['#3ea9fb', '#3ea9fb', '#0468e2']} style={pstyles.linearGradient}>
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Text style={styles.myText}>Join BizCard</Text>
                    <Image style={styles.logoImage} source={require('../../../assets/icon/handshake.png')}/>
                    <Text style={[styles.myText, {fontSize: hp('3%'), marginTop: hp('2%'), alignSelf: 'center'}]}>Create your own Digital Card</Text>
                </View>
                <View style={pstyles.inputContainer}>
                    <View style={pstyles.action}>
                        <Icon 
                            name="account-circle"
                            color='white'
                            size={25}
                        />
                        <TextInput 
                            placeholder="Username"
                            placeholderTextColor="lightgrey"
                            style={[pstyles.textLogin, {
                                color: 'white'
                            }]}
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={pstyles.action}>
                        <Icon 
                            name="mail"
                            color='white'
                            size={25}
                        />
                        <TextInput 
                            placeholder="Email"
                            placeholderTextColor="lightgrey"
                            style={[pstyles.textLogin, {
                                color: 'white'
                            }]}
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={pstyles.action}>
                        <Icon 
                            name="phone"
                            color='white'
                            size={25}
                        />
                        <TextInput 
                            placeholder="Phone"
                            placeholderTextColor="lightgrey"
                            style={[pstyles.textLogin, {
                                color: 'white'
                            }]}
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={pstyles.action}>
                        <Icon 
                            name="lock"
                            color='white'
                            size={25}
                        />
                        <TextInput 
                            placeholder="Password"
                            placeholderTextColor="lightgrey"
                            style={[pstyles.textLogin, {
                                color: 'white'
                            }]}
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'center'}]}>
                        <TouchableOpacity
                            style={pstyles.SubmitButtonStyle}
                            activeOpacity = { .5 }
                            onPress = {() => props.navigation.navigate('Tabs')}
                        >
                            <Text style={pstyles.TextStyle}> Join Now </Text>    
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    </LinearGradient>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: wp('8%')
    },
    myText: {
        fontSize: hp('5%'), // End result looks like the provided UI mockup
        color: 'white'
    },
    logoImage: {
        height: hp('12%'),
        width: wp('30%'),
        marginTop: hp('2%'),
        resizeMode: 'stretch'
    },
    logoContainer: {
        height: hp('40%'), // 70% of height device screen
        paddingTop: hp('8%'),
        alignItems: 'center'
    }
})
export default Register;