import React from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from "react-native-elements";
import { styles as pstyles } from '../../Styles';
import {LinearGradient} from 'expo-linear-gradient';

const FinishRegister = props => {
    return(
        <LinearGradient colors={['#3ea9fb', '#3ea9fb', '#0468e2']} style={pstyles.linearGradient}>
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logoImage} source={require('../../../assets/icon/check-mark.png')}/>
                    <Text style={styles.myText}>Success</Text>
                </View>
                <View style={[pstyles.inputContainer, {marginTop: 0}]}>
                    <Image style={[styles.finshImage, {alignSelf:'center'}]} source={require('../../../assets/icon/finish-img.png')}/>
                    <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'center'}]}>
                        <Text style={{color: "white", textAlign:'center'}}>Thanks for registering in BizCard, complete your Bio and start meet new people</Text>
                    </View>
                    <View style={[pstyles.action, {borderBottomWidth: 0, justifyContent: 'center'}]}>
                        <TouchableOpacity
                            style={pstyles.SubmitButtonStyle}
                            activeOpacity = { .5 }
                        >
                            <Text style={pstyles.TextStyle}> Continue </Text>    
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    container: { flex: 1,paddingHorizontal: wp('8%') },
    logoContainer: {
        height: hp('40%'), // 70% of height device screen
        paddingTop: hp('10%'),
        alignItems: 'center',
        textAlign: ' '
    },
    myText: {
        fontSize: hp('5%'), // End result looks like the provided UI mockup
        color: 'white'
    },
    logoImage: {
        height: hp('15%'),
        width: hp('15%'),
        marginTop: hp('2%'),
        resizeMode: 'stretch'
    },
    finshImage: {
        height: hp('30%'),
        width: hp('30%'),
        marginTop: hp('2%'),
        resizeMode: 'stretch'
    }
});

export default FinishRegister;