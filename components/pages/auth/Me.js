import React from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Svg, { Path } from 'react-native-svg'
import { Container, Content, Button, Icon, ListItem, Left, Right, Body, Switch } from 'native-base';
import BizFooter from '../../BizFooter';

const Me = props => {
    return (
        <Container>
            <View style={styles.svgCurve}>
                <View style={{ backgroundColor: '#2b92f1', height: Dimensions.get('window').height / 5 }}>
                    <Svg
                    height={Dimensions.get('window').height / 6}
                    width="100%"
                    viewBox="0 0 1440 320"
                    style={{ position: 'absolute', top: Dimensions.get('window').height / 6 }}
                    >
                    <Path
                        fill="#2b92f1"
                        d="M0,96L120,128C240,160,480,224,720,218.7C960,213,1200,139,1320,101.3L1440,64L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"
                    />
                    </Svg>
                </View>
            </View>
            <Content>
                <View style={{paddingHorizontal: Dimensions.get('window').width / 30}}>
                    <View style={{paddingTop: Dimensions.get('window').height / 20}}>
                        <View style={styles.card}>
                            <Image
                                style={styles.tinyLogo}
                                source={ require('../../../assets/icon/man-300x300.png') }
                            />
                            <View style={styles.textPlace}>
                                <Text style={styles.cardName}>Afrizal Azhar</Text>
                                <Text style={styles.companyName}>PT. Epsilon Global</Text>
                                <Text style={styles.cardName}>Software Developer</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.cardMenu}>
                        <View style={{flexDirection: 'row', marginBottom: Dimensions.get('window').width / 50}}>
                            <Icon name="share" style={{fontSize: Dimensions.get('window').width / 30, marginRight: 10}}/>
                            <Text style={{fontSize: Dimensions.get('window').width / 30}}>Share my card</Text>
                        </View>
                        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                            <Button info transparent vertical>
                                <Icon name="barcode" />
                                <Text style={styles.textMenu}>QR Code</Text>
                            </Button>
                            <Button info transparent vertical>
                                <Icon name="mail" />
                                <Text style={styles.textMenu}>Email</Text>
                            </Button>
                            <Button info transparent vertical>
                                <Icon name="logo-whatsapp" />
                                <Text style={styles.textMenu}>Whatsapp</Text>
                            </Button>
                            <Button info transparent vertical>
                                <Icon name="more" />
                                <Text style={styles.textMenu}>More</Text>
                            </Button>
                        </View>
                    </View>
                </View>
                <View style={styles.listMenu}>
                    <ListItem icon>
                        <Left>
                            <Button info transparent>
                                <Icon name="star" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Premium Account</Text>
                        </Body>
                        <Right>
                            <TouchableOpacity><Text style={{color: 'red'}}>Upgrade</Text></TouchableOpacity>
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button info transparent>
                                <Icon name="card" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>My Card</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button info transparent>
                                <Icon name="bookmarks" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Contact Backup</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button info transparent>
                                <Icon name="build" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Settings</Text>
                        </Body>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button info transparent>
                                <Icon name="help-circle-outline" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Help & Feedback</Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                </View>
            </Content>
        </Container>
    );
}

const styles = StyleSheet.create({
    svgCurve: {
        position: 'absolute',
        width: Dimensions.get('window').width
    },
    card: {
        flexDirection: 'row',
        padding: 5,
        marginBottom: 10,
        marginTop: Dimensions.get('window').height / 50
    },
    tinyLogo: {
        width: Dimensions.get('window').height / 12,
        height: Dimensions.get('window').height / 12,
        resizeMode: 'stretch'
    },
    textPlace : {
        paddingLeft: 10
    },
    cardName: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: Dimensions.get('window').height / 50
    },
    companyName: {
        color: '#fff',
        fontSize: Dimensions.get('window').height / 55
    },
    cardMenu: {
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        marginBottom: Dimensions.get('window').height / 50,
        padding: Dimensions.get('window').width / 50
    },
    textMenu: {
        color: '#000',
        fontSize: Dimensions.get('window').width / 25
    },
    listMenu: {
        marginTop: Dimensions.get('window').height / 30,
        backgroundColor: '#fff'
    },
    listText: {
        fontSize: Dimensions.get('window').height / 50
    }

})

export default Me;