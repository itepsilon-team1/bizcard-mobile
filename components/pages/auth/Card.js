import React from 'react';
import { Container, Content, List, Header, Left, Title, Button, Body } from 'native-base';
import { View, Text, Dimensions, StyleSheet, TextInput, Image } from 'react-native';
import { Icon as IconR } from "react-native-elements";
import { styles as pstyles } from '../../Styles';

const Cards = props => {
    return (
        <Container>
            <Header style={{backgroundColor: '#2b92f1'}} androidStatusBarColor='#2b92f1' >
                <Left>
                    <Button transparent>
                        <IconR name='arrow-back'
                        color='#fff' />
                    </Button>
                </Left>
                <Body>
                    <Title>Card</Title>
                </Body>
            </Header>
            <Content style={{backgroundColor: '#f7f7f7'}}>
                <View style={styles.searchSection}>
                    <Text>All Cards (20)</Text>
                    <View style={[pstyles.action, {marginTop: Dimensions.get('window').width / 30, backgroundColor: '#fff', padding: 5}]}>
                        <IconR 
                            name="search"
                            color='#9f9f9f'
                            size={Dimensions.get('window').width / 20}
                            style={{paddingTop: 5}}
                        />
                        <TextInput 
                            placeholder="Search"
                            placeholderTextColor="lightgrey"
                            style={[pstyles.textInput, {
                                color: '#000'
                            }]}
                            autoCapitalize="none"
                        />
                    </View>
                </View>
                <List style={{paddingHorizontal: Dimensions.get('window').width / 30}}>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View><View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Image
                            style={styles.tinyLogo}
                            source={ require('../../../assets/icon/man-300x300.png') }
                        />
                        <View style={styles.textPlace}>
                            <Text style={styles.cardName}>Afrizal Azhar</Text>
                            <Text>PT. Epsilon Global</Text>
                            <Text style={styles.cardName}>Software Developer</Text>
                        </View>
                    </View>
                </List>
            </Content>
        </Container>
    )
}

const styles = StyleSheet.create({
    searchSection: {
        marginTop: Dimensions.get('window').height / 50,
        paddingHorizontal: Dimensions.get('window').width / 30,
    },
    card: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 5,
        marginBottom: 10
    },
    tinyLogo: {
        width: 60,
        height: 60,
    },
    textPlace : {
        paddingLeft: 10
    },
    cardName: {
        fontWeight: 'bold'
    }
})

export default Cards;