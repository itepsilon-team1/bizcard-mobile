import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Icon } from "react-native-elements";

const Header = props => {
    return (
        <View style={styles.header}>
            <View style={{flex:1}}>
                <Icon 
                    name="arrow-back"
                    color='white'
                    size={Dimensions.get('window').height / 21}
                    style={{alignSelf: 'flex-start', paddingLeft: 5}}
                />
            </View>
            <View style={{flex:1}}>
                <Text style={styles.headerText}>{props.title}</Text>
            </View>            
            <View style={{flex:1}}>

            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        height: hp('12%'),
        paddingTop: Dimensions.get('window').height / 15,
        backgroundColor: '#3faafc',
        flexDirection: 'row'
    },
    headerText: {
        fontSize: hp('3%'),
        color: '#fff',
        alignSelf: 'center'
    }
})

export default Header;