import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MasterNavigation from './navigation/MasterNaviagtion';

export default function App() {
  return (
    <NavigationContainer>
      <MasterNavigation/>
    </NavigationContainer>
  );
}
